#-------------------------------------------------
#
# Project created by QtCreator 2016-01-13T12:17:47
#
#-------------------------------------------------

QT       += core gui qml quick

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets bluetooth

TARGET = PC_Control
TEMPLATE = app


SOURCES += main.cpp\
    pccontrol.cpp

HEADERS  += \
    pccontrol.h

FORMS    += mainwindow.ui

CONFIG += mobility qtquickcompiler
MOBILITY = 

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    qml/Interface.qml \
    qml/InterfaceButton.qml \
    qml/MenuLine.qml \
    qml/KeyboardPanel.qml \
    qml/MainPanel.qml \
    qml/ConnectionPanel.qml \
    qml/MousePanel.qml \
    qml/KeyboardButton.qml \
    qml/Message.qml

RESOURCES += \
    pics.qrc

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

