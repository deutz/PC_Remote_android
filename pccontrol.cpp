#include "pccontrol.h"

#include <QBluetoothLocalDevice>
#include <QtAndroidExtras/QtAndroid>
#include <QMessageBox>

static const QLatin1String spUuid("{00001101-0000-1000-8000-00805f9b34fb}");

PCControl::PCControl(QWindow *parent) :
    QQuickView(parent)
{
    init();
}

PCControl::~PCControl()
{

}

void PCControl::init()
{
    this->rootContext()->setContextProperty("MainWindow", this);
    setResizeMode(QQuickView::SizeRootObjectToView);
    // load QML
    setSource(QUrl(QStringLiteral("qrc:/qml/Interface.qml")));


    QBluetoothAddress adapter = QBluetoothLocalDevice::allDevices().at(0).address();
    QBluetoothLocalDevice(adapter).powerOn();

//    m_discoveryAgent = new QBluetoothServiceDiscoveryAgent(adapter);
//    m_ddA = new QBluetoothDeviceDiscoveryAgent(adapter);

//    connect(m_ddA, SIGNAL(deviceDiscovered(QBluetoothDeviceInfo)), this, SLOT(deviceDiscovered(QBluetoothDeviceInfo)));
//    m_ddA->start();
//    connect(m_discoveryAgent, SIGNAL(serviceDiscovered(QBluetoothServiceInfo)),
//            this, SLOT(serviceDiscovered(QBluetoothServiceInfo)));
//    m_discoveryAgent->start(QBluetoothServiceDiscoveryAgent::FullDiscovery);

}

void PCControl::makeDevList()
{

    m_discoveredDev.clear();
    QStringList devices = getBluetoothDevices();
    qDebug() << devices;
    foreach(QString dev, devices)
    {
        QBluetoothServiceInfo info;
        info.setServiceName("Serial Port Profile");
        info.setServiceUuid(QBluetoothUuid(spUuid));
        info.setDevice(QBluetoothDeviceInfo(QBluetoothAddress(dev.section(" ", 0, 0)),
                                            dev.section(" ", 1, 1),
                                            0));
        qDebug() << info.device().name() << "  " << info.serviceUuid() << "  " << info.serviceName()
                    << "  " << info.device().address();
        m_discoveredDev.append(info);
        emit newDevice(info.device().name());
    }
    if(m_discoveredDev.size() != 0)
    {
        emit searchComplete();
    }
    else
    {
        emit errorToQML(tr("No paired devices!"));
    }
}

void PCControl::selectRemote(QString name)
{
    foreach(QBluetoothServiceInfo info, m_discoveredDev)
    {
        if(info.device().name() == name)
            m_comp = info;
    }
}

void PCControl::connectToComp()
{
    // Connect to service
    m_socket = new QBluetoothSocket(QBluetoothServiceInfo::RfcommProtocol);
    qDebug() << "Create socket";
    connect(m_socket, SIGNAL(error(QBluetoothSocket::SocketError)),
            this, SLOT(printErr(QBluetoothSocket::SocketError)));
    m_socket->connectToService(m_comp.device().address(), QBluetoothUuid(spUuid));
    if(m_socket->isOpen())
        emit errorToQML(tr("Connected to %1.").arg(m_comp.device().name()));
    qDebug() << "ConnectToService done";
}

void PCControl::sendCommand(QString comm)
{
    if(m_socket)
        m_socket->write(comm.toLocal8Bit());
    else
        emit errorToQML(tr("Connection didn't established."));
}

void PCControl::printErr(QBluetoothSocket::SocketError err)
{
    QMetaEnum eSE = QMetaEnum::fromType<QBluetoothSocket::SocketError>();
    qDebug() << eSE.valueToKey(err);
    // TODO: Add errors output
    emit errorToQML(m_socket->errorString());
}

QStringList PCControl::getBluetoothDevices()
{
    QStringList result;
    QString fmt("%1 %2");

    // Query via Android Java API.
    QAndroidJniObject adapter = QAndroidJniObject::callStaticObjectMethod
            ("android/bluetooth/BluetoothAdapter",
             "getDefaultAdapter",
             "()Landroid/bluetooth/BluetoothAdapter;"); // returns a BluetoothAdapter
    if (checkException("BluetoothAdapter.getDefaultAdapter()", &adapter))
    {
        return result;
    }
    QAndroidJniObject pairedDevicesSet = adapter.callObjectMethod
            ("getBondedDevices",
             "()Ljava/util/Set;"); // returns a Set<BluetoothDevice>
    if (checkException("BluetoothAdapter.getBondedDevices()", &pairedDevicesSet))
    {
        return result;
    }
    jint size = pairedDevicesSet.callMethod<jint>("size");
    checkException("Set<BluetoothDevice>.size()", &pairedDevicesSet);
    if (size > 0)
    {
        QAndroidJniObject iterator = pairedDevicesSet.callObjectMethod
                ("iterator",
                 "()Ljava/util/Iterator;"); // returns an Iterator<BluetoothDevice>
        if (checkException("Set<BluetoothDevice>.iterator()", &iterator))
        {
            return result;
        }
        for (int i = 0; i < size; i++)
        {
            QAndroidJniObject dev = iterator.callObjectMethod
                    ("next",
                     "()Ljava/lang/Object;"); // returns a BluetoothDevice
            if (checkException("Iterator<BluetoothDevice>.next()", &dev))
            {
                continue;
            }
            QString address = dev.callObjectMethod
                    ("getAddress",
                     "()Ljava/lang/String;").toString(); // returns a String
            QString name = dev.callObjectMethod
                    ("getName",
                     "()Ljava/lang/String;").toString(); // returns a String
            result.append(fmt.arg(address).arg(name));
        }
    }
    return result;
}

bool PCControl::checkException(const char* method, const QAndroidJniObject* obj)
{
    static QAndroidJniEnvironment env;
    bool result = false;
    if (env->ExceptionCheck())
    {
        qCritical("Exception in %s", method);
        env->ExceptionDescribe();
        env->ExceptionClear();
        result = true;
    }
    if (!(obj == NULL || obj->isValid()))
    {
        qCritical("Invalid object returned by %s", method);
        result = true;
    }
    return result;
}

//void PCControl::serviceDiscovered(const QBluetoothServiceInfo &serviceInfo)
//{
//    qDebug() << "\tDiscovered service on"
//             << serviceInfo.device().name() << serviceInfo.device().address().toString()
//    /*qDebug()*/ << "\tService name:" << serviceInfo.serviceName()
//    /*qDebug()*/ << "\tDescription:"
//             << serviceInfo.serviceDescription()
//    /*qDebug()*/ << "\tProvider:"
//             << serviceInfo.serviceProvider();
//}

//void PCControl::deviceDiscovered(const QBluetoothDeviceInfo &deviceInfo)
//{
//    qDebug() << deviceInfo.address() << "  " << deviceInfo.name() << "  " << deviceInfo.deviceUuid()
//             << "  " << deviceInfo.rssi();
//}

bool operator==(const QBluetoothServiceInfo a, const QBluetoothServiceInfo b)
{
    return a.device().name() == b.device().name();
}


