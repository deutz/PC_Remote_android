#pragma once

#include <QWidget>
#include <QQuickView>
#include <QQmlContext>

#include <QtAndroidExtras/QAndroidJniObject>
#include <QtAndroidExtras/QAndroidJniEnvironment>

#include <QBluetoothSocket>
#include <QBluetoothServiceInfo>
#include <QBluetoothDeviceInfo>
//#include <QBluetoothServiceDiscoveryAgent>
//#include <QBluetoothDeviceDiscoveryAgent>

#include <QDebug>


class PCControl : public QQuickView
{
    Q_OBJECT
public:
    PCControl(QWindow *parent = 0);
    ~PCControl();

    void init();

signals:
    void newDevice(QString name);

    void searchComplete();

    void errorToQML(QString error);

public slots:
    void makeDevList();

    void selectRemote(QString name);

    void connectToComp();

    void sendCommand(QString comm);

    void printErr(QBluetoothSocket::SocketError err);

    QStringList getBluetoothDevices();

    bool checkException(const char* method, const QAndroidJniObject* obj);


//    void serviceDiscovered(const QBluetoothServiceInfo &serviceInfo);
//    void deviceDiscovered(const QBluetoothDeviceInfo &deviceInfo);

private:

    //QBluetoothServiceDiscoveryAgent *m_discoveryAgent;
    QBluetoothServiceInfo m_comp;
    QBluetoothSocket* m_socket;

    QVector<QBluetoothServiceInfo> m_discoveredDev;

//    QBluetoothServiceDiscoveryAgent* m_discoveryAgent;
//    QBluetoothDeviceDiscoveryAgent* m_ddA;
};

bool operator==(const QBluetoothServiceInfo a, const QBluetoothServiceInfo b);
