import QtQuick 2.5
import QtQuick.Layouts 1.1

Rectangle {
    id: button

    property string name: "A"
    property string dispName: button.name
    property bool isToggled: false
    signal pressed
    signal released
    signal clicked

    //width: parent.height
    //height: width
    color: "transparent"

    Layout.fillWidth: true
    Layout.fillHeight: true

    Rectangle {
        id: bRect
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
        radius: 8
        color: "#00aeef"
        border.color: "white"

        Text {
            anchors.centerIn: parent
            text: button.dispName
            font.pixelSize: parent.height / 4
            font.bold: true
            color: "white"
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            button.clicked()
            if(name == "Caps" || name == "Shift" || name == "Alt" || name == "Ctrl"){
                if(button.isToggled) {
                    button.parent.parent.parent.parent.keyPressed(button.name + "|u")
                }
                else {
                    button.parent.parent.parent.parent.keyPressed(button.name + "|d")
                }
                button.isToggled = !button.isToggled
                if(button.isToggled) {
                    bRect.color = "darkBlue"
                }
                else {
                    bRect.color = "#00aeef"
                }
            }
            else {
                button.parent.parent.parent.parent.keyPressed(button.name)
            }
        }
        onPressed: {
            button.pressed()
            bRect.border.width = 5
        }
        onReleased: {
            button.released()
            bRect.border.width = 0
        }
    }

    state: parent.parent.parent.state

    onStateChanged: {
        if(state == "EN")
        {
            button.dispName = button.name
        }
        else
        {
            if(button.name == "Q")
            {
                button.dispName = "Й"
            }
            else if(button.name == "W")
            {
                button.dispName = "Ц"
            }
            else if(button.name == "E")
            {
                button.dispName = "У"
            }
            else if(button.name == "R")
            {
                button.dispName = "К"
            }
            else if(button.name == "T")
            {
                button.dispName = "Е"
            }
            else if(button.name == "Y")
            {
                button.dispName = "Н"
            }
            else if(button.name == "U")
            {
                button.dispName = "Г"
            }
            else if(button.name == "I")
            {
                button.dispName = "Ш"
            }
            else if(button.name == "O")
            {
                button.dispName = "Щ"
            }
            else if(button.name == "P")
            {
                button.dispName = "З"
            }
            else if(button.name == "[")
            {
                button.dispName = "Х"
            }
            else if(button.name == "]")
            {
                if(state == "RU") {
                    button.dispName = "Ъ"
                }
                else {
                    button.dispName = "Ї"
                }
            }
            else if(button.name == "A")
            {
                button.dispName = "Ф"
            }
            else if(button.name == "S")
            {
                if(state == "RU") {
                    button.dispName = "Ы"
                }
                else {
                    button.dispName = "І"
                }
            }
            else if(button.name == "D")
            {
                button.dispName = "В"
            }
            else if(button.name == "F")
            {
                button.dispName = "А"
            }
            else if(button.name == "G")
            {
                button.dispName = "П"
            }
            else if(button.name == "H")
            {
                button.dispName = "Р"
            }
            else if(button.name == "J")
            {
                button.dispName = "О"
            }
            else if(button.name == "K")
            {
                button.dispName = "Л"
            }
            else if(button.name == "L")
            {
                button.dispName = "Д"
            }
            else if(button.name == ";")
            {
                button.dispName = "Ж"
            }
            else if(button.name == "'")
            {
                if(state == "RU") {
                    button.dispName = "Э"
                }
                else {
                    button.dispName = "Є"
                }
            }
            else if(button.name == "Z")
            {
                button.dispName = "Я"
            }
            else if(button.name == "X")
            {
                button.dispName = "Ч"
            }
            else if(button.name == "C")
            {
                button.dispName = "С"
            }
            else if(button.name == "V")
            {
                button.dispName = "М"
            }
            else if(button.name == "B")
            {
                button.dispName = "И"
            }
            else if(button.name == "N")
            {
                button.dispName = "Т"
            }
            else if(button.name == "M")
            {
                button.dispName = "Ь"
            }
            else if(button.name == ",")
            {
                button.dispName = "Б"
            }
            else if(button.name == ".")
            {
                button.dispName = "Ю"
            }
            else if(button.name == "/")
            {
                button.dispName = "."
            }
        }
    }

}

