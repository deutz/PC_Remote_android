import QtQuick 2.5

Rectangle {
    id: button

    property url imgSource
    property int imgWidth: img.paintedWidth// width
    property int imgHeight: img.paintedHeight// height
    signal pressed
    signal released
    signal clicked

    width: parent.height
    height: width
    color: "transparent"

    // TODO: add shadow

    Rectangle {
        id: imgRect
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
        color: "transparent"
        Image {
            id: img
            source: imgSource
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: button.clicked()
        onPressed: {
            button.pressed()
            //button.color = "white"
            imgRect.height = imgRect.height - 10
            imgRect.width = imgRect.width - 10
           // button.anchors.centerIn = button.parent
        }
        onReleased: {
            button.released()
            //button.color = "transparent"
            imgRect.height = imgRect.height + 10
            imgRect.width = imgRect.width + 10
            //button.anchors.fill = button.parent
        }
    }
}

