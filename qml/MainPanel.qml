import QtQuick 2.5
import QtQuick.Window 2.0
import QtQuick.Layouts 1.1

Rectangle {
    id: root
    property int itemSize: 100
    property int shadowMarg: 12
    width: root.width
    height: root.height

    Image {
        source: "../pics/background.jpg"
        anchors.fill: parent

        fillMode: Image.PreserveAspectCrop
    }

    Rectangle {
        id: headerMain
        width: parent.width
        height: itemSize
        anchors.right: parent.right

        visible: true
        color: "black"
        opacity: 0.5
    }

    InterfaceButton {
        id: menuButton
        width: itemSize
        imgSource: "../pics/menu.png"
        onClicked: {
            menuRect.state = "menuOpened"
        }
    }

    Rectangle {
        id: mainButtons
        height: root.height / 2
        width: root.width / 2
        anchors.centerIn: parent
        color: "transparent"

        ColumnLayout {
            anchors.fill: parent

            Rectangle {
                Layout.fillWidth: true
                height: itemSize
                color: "transparent"//"green"

                InterfaceButton {
                    id: intButtConn
                    width: parent.height
                    imgSource: "../pics/connect.png"
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    onClicked: {
                        stack.push(devices)
                        MainWindow.makeDevList()
                    }

                    Rectangle {
                        width: parent.imgWidth
                        height: parent.imgHeight
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.horizontalCenterOffset: shadowMarg
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.verticalCenterOffset: shadowMarg
                        z: -10
                        color: "black"
                        radius: 10
                        opacity: 0.5
                    }
                }

                InterfaceButton {
                    id: intButtMouse
                    width: parent.height
                    imgSource: "../pics/mouse.png"
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    onClicked: {
                        stack.push(mousePad)
                    }

                    Rectangle {
                        width: parent.imgWidth
                        height: parent.imgHeight
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.horizontalCenterOffset: shadowMarg
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.verticalCenterOffset: shadowMarg
                        z: -10
                        color: "black"
                        radius: 10
                        opacity: 0.5
                    }
                }

            }

            Rectangle {
                Layout.fillWidth: true
                height: itemSize
                color: "transparent"//"darkGreen"

                InterfaceButton {
                    id: intButtPlay

                    height: parent.height
                    width: parent.width
                    anchors.centerIn: parent

                    imgSource: "../pics/play.png"

                    onClicked: {
                        MainWindow.sendCommand("play")
                    }

                    Rectangle {
                        width: parent.imgWidth
                        height: parent.imgHeight
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.horizontalCenterOffset: shadowMarg
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.verticalCenterOffset: shadowMarg
                        z: -10
                        color: "black"
                        radius: 10
                        opacity: 0.5
                    }
                }

            }

            Rectangle {
                height: itemSize
                color:  "transparent"//"lightGreen"
                Layout.fillWidth: true

                InterfaceButton {
                    id: intButtSM
                    width: parent.height
                    imgSource: "../pics/sound_m.png"
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter

                    onClicked: {
                        MainWindow.sendCommand("sm")
                    }

                    Rectangle {
                        width: parent.imgWidth
                        height: parent.imgHeight
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.horizontalCenterOffset: shadowMarg
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.verticalCenterOffset: shadowMarg
                        z: -10
                        color: "black"
                        radius: 10
                        opacity: 0.5
                    }
                }

                InterfaceButton {
                    id: intButtSP
                    width: parent.height
                    imgSource: "../pics/sound_p.png"
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter

                    onClicked: {
                        MainWindow.sendCommand("sp")
                    }

                    Rectangle {
                        width: parent.imgWidth
                        height: parent.imgHeight
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.horizontalCenterOffset: shadowMarg
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.verticalCenterOffset: shadowMarg
                        z: -10
                        color: "black"
                        radius: 10
                        opacity: 0.5
                    }
                }

            }
        }
    }

    Rectangle {
        id: menuRect
        color: "transparent"
        height: root.height
        width: root.width

        onStateChanged: {
            if(state == "menuOpened")
            {
                menuButton.enabled = false
                intButtConn.enabled = false
                intButtMouse.enabled = false
                intButtPlay.enabled = false
                intButtSM.enabled = false
                intButtSP.enabled = false
            }
            else
            {
                mlMain.state = "pressed"
                menuButton.enabled = true
                intButtConn.enabled = true
                intButtMouse.enabled = true
                intButtPlay.enabled = true
                intButtSM.enabled = true
                intButtSP.enabled = true
            }
        }

        Rectangle {
            width: root.width  * 5 / 6
            height: root.height
            color: "black"
            opacity: 0.5
        }

        Rectangle {
            id: linesRect
            width: root.width  * 5 / 6
            height: root.height
            color: "transparent"

            ColumnLayout {
                MenuLine {
                    id: mlMain
                    lineName: "Main screen"
                    height: itemSize
                    width: root.width  * 5 / 6
                    state: "pressed"
                    onClicked: {
                        menuRect.state = "menuClosed"
                        state = "pressed"
                    }
                }

                MenuLine {
                    lineName: "Connect to..."
                    height: itemSize
                    width: root.width  * 5 / 6

                    onClicked: {
                        menuRect.state = "menuClosed"
                        stack.push(devices)
                        MainWindow.makeDevList()
                    }

                    onPressed: mlMain.state = "normal"
                }

                MenuLine {
                    lineName: "Mouse"
                    height: itemSize
                    width: root.width  * 5 / 6

                    onClicked: {
                        menuRect.state = "menuClosed"
                        stack.push(mousePad)
                    }

                    onPressed: mlMain.state = "normal"
                }

                MenuLine {
                    lineName: "Keyboard"
                    height: itemSize
                    width: root.width  * 5 / 6

                    onClicked: {
                        menuRect.state = "menuClosed"
                        stack.push(keyboard)
                    }

                    onPressed: mlMain.state = "normal"
                }

                MenuLine {
                    lineName: "Exit"
                    height: itemSize
                    width: root.width  * 5 / 6
                    onClicked: MainWindow.close()

                    onPressed: mlMain.state = "normal"
                }
            }
        }

        MouseArea {
            width: root.width - linesRect.width
            height: root.height
            anchors.right: parent.right

            onClicked: {
                menuRect.state = "menuClosed"
            }
        }

        states: [
            State {
                name: "menuClosed"
                PropertyChanges {
                    target: menuRect
                    x: -root.width
                }
            },
            State {
                name: "menuOpened"
                PropertyChanges {
                    target: menuRect
                    x: 0
                }
            }
        ]

        state: "menuClosed"

        transitions: [
            Transition {
                from: "menuClosed"
                to: "menuOpened"
                NumberAnimation {
                    properties: "x"
                    easing.type: Easing.InSine
                    duration: 300
                }
            },
            Transition {
                from: "menuOpened"
                to: "menuClosed"
                NumberAnimation {
                    properties: "x"
                    easing.type: Easing.InSine
                    duration: 300
                }
            }
        ]
    }

}

