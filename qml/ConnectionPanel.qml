import QtQuick 2.5

Rectangle {
    id: root
    property int itemSize: 100
    color: "transparent"

    Component.onCompleted: parent.parent.message("Pair to device before connect.")

    ListModel {
        id: devModel
    }

    Image {
        source: "../pics/background.jpg"
        width: root.width
        height: root.height
        anchors.right: parent.right

        fillMode: Image.PreserveAspectCrop
    }

    Rectangle {
        id: topPanelD
        width: parent.width
        height: itemSize
        anchors.right: parent.right

        visible: true
        color: "black"
        opacity: 0.5
    }

    InterfaceButton {
        id: returnButtonD
        width: itemSize
        imgSource: "../pics/back.png"
        onClicked: stack.pop()
    }

    Text {
        // TODO: change to something else
        id: statusText
        width: parent.width - itemSize
        font.pixelSize: itemSize / 3
        anchors.left: returnButtonD.right
        anchors.leftMargin: itemSize / 5
        anchors.verticalCenter: topPanelD.verticalCenter
        text: "Searching..."
        color: "white"
        fontSizeMode: Text.Fit

    }

    Rectangle {
        height: parent.height - itemSize
        width: parent.width
        anchors.top: topPanelD.bottom
        color: "transparent"
        ListView {
            height: parent.height - 30
            width: parent.width - 30
            model: devModel
            spacing: 10
            anchors.centerIn: parent
            delegate: Rectangle {
                color: "transparent"
                height: itemSize
                width: parent.width
                border.color: "white"
                border.width: 5

                Row {
                    anchors.left: parent.left
                    anchors.leftMargin: 20
                    anchors.verticalCenter: parent.verticalCenter
                    spacing: 10
                    Text { text: "#" + number; font.pixelSize: itemSize / 3 }
                    Text { text: name; font.pixelSize: itemSize / 3 }
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        MainWindow.selectRemote(name)
                        MainWindow.connectToComp()
                        stack.pop()
                    }
                }
            }

        }
    }

    Connections {
        target: MainWindow
        onNewDevice: devModel.append({"number":devModel.count + 1, "name":name})
        onSearchComplete: statusText.text = "Choose device"
    }

}

