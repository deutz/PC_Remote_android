import QtQuick 2.5

Rectangle {
    id: root
    property int itemSize: 100
    //width: root.width
    //height: root.height

    Rectangle {
        id: topPanelM
        width: parent.width
        height: itemSize
        anchors.right: parent.right

        visible: true
        color: "black"
        opacity: 0.5
    }

    InterfaceButton {
        id: returnButtonM
        width: itemSize
        imgSource: "../pics/back.png"
        onClicked: stack.pop()
    }

    Rectangle {
        anchors.top: topPanelM.bottom
        height: root.height - topPanelM.height
        width: root.width

        Column {
            anchors.centerIn: parent
            spacing: itemSize / 5
            Text {
                text: "One tap for left click"
                font.pixelSize: itemSize / 3
                font.italic: true
                color: "#00aeef"
            }
            Text {
                text: "Double tap for right click"
                font.pixelSize: itemSize / 3
                font.italic: true
                color: "#00aeef"
            }
        }

        MouseArea {
            id: mouseField
            property int xpos: 0
            property int ypos: 0
            property int xPress: 0
            property int yPress: 0
            property int sendCounter: 0
            anchors.fill: parent

            onPressed: {
                mouseField.xpos = mouse.x
                mouseField.ypos = mouse.y
                xPress = mouse.x
                yPress = mouse.y
            }

            onPressAndHold: {
            }

            onClicked: {
                if(mouse.x == mouseField.xPress
                        && mouse.y == mouseField.yPress) {
                    MainWindow.sendCommand("mlc");
                }
            }

            onDoubleClicked: {
                MainWindow.sendCommand("mrc");
            }

            onPositionChanged: {
                if((mouseField.sendCounter % 2) == 1)
                {
                    MainWindow.sendCommand("m|" + (mouse.x - mouseField.xpos)
                                           + "|" + (mouse.y - mouseField.ypos))
                }
                mouseField.xpos = mouse.x
                mouseField.ypos = mouse.y
                mouseField.sendCounter++;
            }
        }
    }
}

