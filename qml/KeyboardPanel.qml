import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4

Rectangle {
    id: root
    property int itemSize: /*50//*/100
    signal keyPressed(string key)
    //width: 600
    //height: 300

    Rectangle {
        id: topPanelK
        width: parent.width
        height: itemSize
        anchors.right: parent.right

        color: "black"
        opacity: 0.5
    }

    InterfaceButton {
        id: returnButtonK
        width: itemSize
        imgSource: "../pics/back.png"
        onClicked: stack.pop()
    }

    Rectangle {
        id: keyboardRect
        anchors.top: topPanelK.bottom
        height: root.height - topPanelK.height
        width: root.width

        state: "EN"
        states: [
            State {
                name: "EN"
            },
            State {
                name: "RU"
            },
            State {
                name: "UA"
            }
        ]

        ColumnLayout {
            anchors.fill: parent
            anchors.margins: 5
            spacing: 5

            RowLayout {
                Layout.preferredHeight: keyboardRect.height / 5
                Layout.minimumHeight: keyboardRect.height / 10
                Layout.fillHeight: true
                width: keyboardRect.width
                KeyboardButton {
                    name: "Esc"
                }
                KeyboardButton {
                    name: "1"

                }
                KeyboardButton {
                    name: "2"

                }
                KeyboardButton {
                    name: "3"

                }
                KeyboardButton {
                    name: "4"

                }
                KeyboardButton {
                    name: "5"

                }
                KeyboardButton {
                    name: "6"

                }
                KeyboardButton {
                    name: "7"

                }
                KeyboardButton {
                    name: "8"

                }
                KeyboardButton {
                    name: "9"

                }
                KeyboardButton {
                    name: "0"

                }
                KeyboardButton {
                    name: "-"

                }
                KeyboardButton {
                    name: "="

                }
                KeyboardButton {
                    name: "Back"
                    Layout.minimumWidth: keyboardRect.width / 15
                    Layout.maximumWidth: keyboardRect.width / 10
                }
            }

            RowLayout {
                Layout.preferredHeight: keyboardRect.height / 5
                Layout.minimumHeight: keyboardRect.height / 10
                Layout.fillHeight: true
                width: keyboardRect.width
                KeyboardButton {
                    name: "Tab"

                }
                KeyboardButton {
                    name: "Q"

                }
                KeyboardButton {
                    name: "W"

                }
                KeyboardButton {
                    name: "E"

                }
                KeyboardButton {
                    name: "R"

                }
                KeyboardButton {
                    name: "T"

                }
                KeyboardButton {
                    name: "Y"

                }
                KeyboardButton {
                    name: "U"

                }
                KeyboardButton {
                    name: "I"

                }
                KeyboardButton {
                    name: "O"

                }
                KeyboardButton {
                    name: "P"

                }
                KeyboardButton {
                    name: "["

                }
                KeyboardButton {
                    name: "]"

                }
            }

            RowLayout {
                Layout.preferredHeight: keyboardRect.height / 5
                Layout.minimumHeight: keyboardRect.height / 10
                Layout.fillHeight: true
                width: keyboardRect.width
                KeyboardButton {
                    name: "Caps"
                    Layout.minimumWidth: keyboardRect.width / 15
                    Layout.maximumWidth: keyboardRect.width / 10
                }
                KeyboardButton {
                    name: "A"

                }
                KeyboardButton {
                    name: "S"

                }
                KeyboardButton {
                    name: "D"

                }
                KeyboardButton {
                    name: "F"

                }
                KeyboardButton {
                    name: "G"

                }
                KeyboardButton {
                    name: "H"

                }
                KeyboardButton {
                    name: "J"

                }
                KeyboardButton {
                    name: "K"

                }
                KeyboardButton {
                    name: "L"

                }
                KeyboardButton {
                    name: ";"

                }
                KeyboardButton {
                    name: "'"

                }
            }

            RowLayout {
                Layout.preferredHeight: keyboardRect.height / 5
                Layout.minimumHeight: keyboardRect.height / 10
                Layout.fillHeight: true
                width: keyboardRect.width
                KeyboardButton {
                    name: "Shift"
                    Layout.minimumWidth: keyboardRect.width / 15
                    Layout.maximumWidth: keyboardRect.width / 8
                }
                KeyboardButton {
                    name: "Z"

                }
                KeyboardButton {
                    name: "X"

                }
                KeyboardButton {
                    name: "C"

                }
                KeyboardButton {
                    name: "V"

                }
                KeyboardButton {
                    name: "B"

                }
                KeyboardButton {
                    name: "N"

                }
                KeyboardButton {
                    name: "M"

                }
                KeyboardButton {
                    name: ","

                }
                KeyboardButton {
                    name: "."

                }
                KeyboardButton {
                    name: "/"

                }
            }

            RowLayout {
                Layout.preferredHeight: keyboardRect.height / 5
                Layout.minimumHeight: keyboardRect.height / 10
                Layout.fillHeight: true
                width: keyboardRect.width
                spacing: 10

                KeyboardButton {
                    name: "Ctrl"

                }
                KeyboardButton {
                    name: "Win"

                }
                KeyboardButton {
                    name: "Alt"

                }
                KeyboardButton {
                    name: " "
                    Layout.minimumWidth: keyboardRect.width / 5
                    Layout.maximumWidth: keyboardRect.width / 3
                }
                KeyboardButton {
                    name: "<"

                }
                KeyboardButton {
                    name: ">"

                }
                KeyboardButton {
                    name: "Lang"
                    onClicked: {
                        if(keyboardRect.state == "EN") {
                            keyboardRect.state = "RU"
                        }
                        else if(keyboardRect.state == "RU") {
                            keyboardRect.state = "UA"
                        }
                        else if(keyboardRect.state == "UA") {
                            keyboardRect.state = "EN"
                        }
                    }
                }
                KeyboardButton {
                    name: "Enter"

                }
            }
        }
    }
}


