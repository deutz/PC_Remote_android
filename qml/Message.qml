import QtQuick 2.5

Rectangle {
    id: root
    color: "transparent"
    property int itemSize: 0
    property string messageText: "Message"
    opacity: 0

    property int transTime: 1000

    onMessageTextChanged: {
        if(messageText != "") {
            timer.restart()
            root.state = "showed"
            root.width = messageText.length * (itemSize / 5)
            root.height = itemSize / 2
        }
    }

    Rectangle {
        anchors.fill: parent
        color: "black"
        opacity: 0.5
        radius: 20
    }

    Text {
        id: textField
        text: messageText
        color: "white"
        font.pixelSize: itemSize / 3
        anchors.centerIn: parent
    }

    state: ""

    states: [
        State {
            name: "showed"
            PropertyChanges {
                target: root
                opacity: 1
//                width: messageText.length * (itemSize / 5)
//                height: itemSize / 2
            }
        }
    ]

    transitions: [
        Transition {
            from: "showed"
            to: ""
            PropertyAnimation {
                properties: "opacity"
                easing.type: Easing.Linear
                duration: transTime
            }
        }
    ]

    Timer {
        id: timer
        interval: 2500
        onTriggered: {
            root.state = ""
            cleaner.start()
        }
    }

    Timer {
        id: cleaner
        interval: transTime
        onTriggered: messageText = ""
    }

}

