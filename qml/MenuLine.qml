import QtQuick 2.5

Rectangle {
    id: lineRect

    property string lineName: ""
    property string lineState: lineRect.state
    property bool isUnchanging: false
    signal pressed
    signal released
    signal clicked
    signal exited

    color: "transparent"

    Rectangle {
        id: headRect
        height: parent.height
        width: height
        anchors.left: parent.left

        color: "transparent"
    }

    Rectangle {
        id: textRect
        height: parent.height
        width: parent.width - headRect.width
        anchors.left: headRect.right

        color: "transparent"
        border.width: 2

        Text {
            text: /*"Main menu"//*/lineRect.lineName
            color: /*"black"//*/"white"
            font.pixelSize: parent.height / 3
            //font.bold: true
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: 20
        }
    }


    MouseArea {
        anchors.fill: parent
        onClicked: lineRect.clicked()
        onPressed: {
            lineRect.pressed()
            if(!isUnchanging) {
                lineRect.state = "pressed"
            }
        }
        onReleased: {
            lineRect.released()
            if(!isUnchanging) {
                lineRect.state = "normal"
            }
        }
        onExited: lineRect.exited()

    }

    state: "normal"

    states: [
        State {
            name: "normal"
            PropertyChanges {
                target: headRect
                color: "transparent"
            }
            PropertyChanges {
                target: textRect
                border.color: "transparent"
            }
        },
        State {
            name: "pressed"
            PropertyChanges {
                target: headRect
                color: "#00aeef"
            }
            PropertyChanges {
                target: textRect
                border.color: "#00aeef"
            }
        }
    ]

    transitions: [
        Transition {
            from: "pressed"
            to: "normal"

            ColorAnimation {
                from: "#00aeef"
                to: "transparent"
                duration: 700
                easing.type: Easing.InExpo
            }
        }
    ]
}

