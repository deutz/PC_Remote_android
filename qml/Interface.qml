import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4

Rectangle {
    width: /*300//*/Screen.width
    height: /*600//*/Screen.height
    id: root
    property int itemSize: /*50//*/100

    signal message(string m)
    onMessage: toast.messageText = m

    Connections {
        target: MainWindow
        onErrorToQML: root.message(error)
    }

    Rectangle {
        focus: true
        visible: false
        Keys.onReleased: {
            if (event.key === Qt.Key_Back)
            {
                event.accepted = true
                if(stack.depth > 1)
                    stack.pop()
                else
                    MainWindow.close()
            }
        }
    }

    StackView {
        id: stack
        initialItem: main
        anchors.fill: parent
        delegate: StackViewDelegate {

            function transitionFinished(properties)
            {
                properties.exitItem.x = 0
                properties.exitItem.y = 0
            }

            popTransition: StackViewTransition {
                PropertyAnimation {
                    target: exitItem
                    property: "x"
                    from: 0
                    to: -target.width
                    duration: 300
                }
            }

            pushTransition: StackViewTransition {
                PropertyAnimation {
                    target: enterItem
                    property: "x"
                    from: -target.width
                    to: 0
                    duration: 300
                }

            }
        }
    }

    Component {
        id: main

        MainPanel {
            height: root.height
            width: root.width
            itemSize: root.itemSize
        }
    }

    Component {
        id: mousePad

        MousePanel {
            height: root.height
            width: root.width
            itemSize: root.itemSize
        }

    }

    Component {
        id: keyboard

        KeyboardPanel {
            height: root.height
            width: root.width
            itemSize: root.itemSize

            onKeyPressed: {
                MainWindow.sendCommand("k|" + key)
            }
        }
    }

    Component {
        id: devices

        ConnectionPanel {
            height: root.height
            width: root.width
            itemSize: root.itemSize
        }
    } // Component

    Message {
        id: toast
        itemSize: root.itemSize

        anchors.horizontalCenter: root.horizontalCenter
        anchors.bottom: root.bottom
        anchors.bottomMargin: itemSize
    }

}

